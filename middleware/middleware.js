const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = {
    // Middleware untuk mengecek apakah sudah login atau belum
    isSiswa: (req, res, next) => {
        try {
            let token = req.headers['x-access-token'] || req.headers['authorization']
            if (token == null) {
                res.status(400)
                res.json({
                    status: 400,
                    message: 'token kosong'
                })
            }
            if (token.startsWith('Bearer ')) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }
            if (token) {
                var decoded = jwt.verify(token, process.env.HASH);
                if (decoded.user.user_role == 2) {
                    next();
                } else {
                    res.status(400).json({
                        status: 400,
                        message: 'Anda bukan siswa'
                    });
                }
            } else {
                res.status(400).json({
                    status: 400,
                    message: 'Token is Invalid'
                });
            }
        } catch (err) {
            res.status(400).json({
                status: err.statusCode,
                message: err.message.toString()
            });
        }
    },
    // Middleware untuk mengecek apakah user sudah login atau belum dengan role superadmin
    isAdmin: (req, res, next) => {
        try {
            let token = req.headers['x-access-token'] || req.headers['authorization']
            if (token == null) {
                res.status(400)
                res.json({
                    status: 400,
                    message: 'token kosong'
                })
            }
            if (token.startsWith('Bearer ')) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }
            if (token) {
                var decoded = jwt.verify(token, process.env.HASH);
                req.account = decoded;
                if (decoded.user.user_role === 1) {
                    next()
                } else {
                    res.status(400)
                    res.json({
                        status: 400,
                        message: 'Unauthorized karena bukan admin'
                    })
                }
            } else {
                res.status(400).json({
                    status: 400,
                    message: 'Token is Invalid'
                });
            }
        } catch (err) {
            res.status(400).json({
                status: err.statusCode,
                message: err.message.toString()
            })
        }
    }
}