var mysql = require("mysql");
require('dotenv').config();

var db = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
    database: process.env.DB_DATABASE
});

db.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

module.exports = db;