var db = require("../database/db_config");
const express = require("express");
const middleware = require("../middleware/middleware")
const nodemailer = require('nodemailer');
require('dotenv').config();

const pendaftaranRouter = express.Router()
pendaftaranRouter.use(express.json())

pendaftaranRouter.route("/:csiswaId")
    .get(middleware.isSiswa, (req, res) => {
        db.connect(function (err) {
            let sql = `SELECT m_csiswa.csiswa_nama, m_csiswa.csiswa_foto, m_penjurusan.penjurusan_nama FROM m_csiswa INNER JOIN m_penjurusan ON m_csiswa.ciswa_penjurusanid = m_penjurusan.penjurusan_id WHERE m_csiswa.csiswa_userid = '${req.params.csiswaId}'`
            db.query(sql, function (err, result) {
                if (err) {
                    res.status(err.code)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: err.code,
                        message: err.message
                    })
                } else {
                    if (result[0] == null) {
                        let sql = `SELECT csiswa_nama, csiswa_foto FROM m_csiswa WHERE csiswa_userid = ${req.params.csiswaId}`
                        db.query(sql, function (err, result) {
                            if (err) {
                                res.status(err.code)
                                res.setHeader('Content-Type', 'application/json')
                                res.json({
                                    status: err.code,
                                    message: err.message
                                })
                            } else {
                                res.status(200)
                                res.setHeader('Content-Type', 'application/json')
                                res.json({
                                    status: 200,
                                    message: "Berhasil ambil info siswa tanpa jurusan",
                                    data: result[0]
                                })
                            }
                        })
                    } else {
                        res.status(200)
                        res.setHeader('Content-Type', 'application/json')
                        res.json({
                            status: 200,
                            message: "Berhasil ambil info siswa",
                            data: result[0]
                        })
                    }
                }
            })
        })
    })
    .put((req, res) => {
        db.connect(function (err) {
            if (req.body.penjurusan_id == null || req.body.penjurusan_id == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'id jurusan kosong!'
                })
            }
            if (req.body.id == null || req.body.id == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'id kosong!'
                })
            }
            if (req.body.email == null || req.body.email == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'email kosong!'
                })
            }
            var count = 0
            var max = 0
            var namaJurusan = ""
            let sql = `SELECT * FROM m_penjurusan WHERE penjurusan_id = ${req.body.penjurusan_id}`
            db.query(sql, function (err, penjurusan) {
                max = penjurusan[0].penjurusan_kuota
                namaJurusan = penjurusan[0].penjurusan_nama
                let sql = `SELECT COUNT(*) FROM m_csiswa WHERE ciswa_penjurusanid = '${req.body.penjurusan_id}`
                db.query(sql, function (err, result) {
                    count = result
                    let sql = `UPDATE m_csiswa SET ciswa_penjurusanid = '${req.body.penjurusan_id}' WHERE csiswa_id = '${req.body.id}'`
                    db.query(sql, function (err, result) {
                        if (err) {
                            res.status(err.code)
                            res.setHeader('Content-Type', 'application/json')
                            res.json({
                                status: err.code,
                                message: err.message
                            })
                        } else {
                            var transport = nodemailer.createTransport({
                                host: "mail.resie.id",
                                port: 465,
                                secure: true,
                                auth: {
                                    user: process.env.EMAIL_USER,
                                    pass: process.env.EMAIL_PASS
                                }
                            });
                            var mailOptions = {
                                from: `"Chevalier" < ${process.env.EMAIL_USER} >`,
                                to: req.body.email,
                                subject: 'Info Pendaftaran Jurusan',
                                text: `Selamat Anda berhasil terdaftar Jurusan ${namaJurusan}`,
                            };
                            transport.sendMail(mailOptions, (error, info) => {
                                if (error) {
                                    res.status(400)
                                    res.setHeader('Content-Type', 'application/json')
                                    res.json({
                                        status: 400,
                                        message: error.message
                                    })
                                } else {
                                    res.status(200)
                                    res.setHeader('Content-Type', 'application/json')
                                    res.json({
                                        status: 200,
                                        message: "Berhasil mendaftar jurusan"
                                    })
                                }
                            });

                        }
                    })
                })
            })
        })
    })
    .post((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method POST'
        })
    })
    .delete((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method DELETE'
        })
    })

pendaftaranRouter.route("/admin/:csiswaId")
    .get((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method GET'
        })
    })
    .post((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method POST'
        })
    })
    .put((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method GET'
        })
    })
    .delete((req, res) => {
        db.connect(function (err) {
            let sql = `DELETE FROM m_csiswa WHERE csiswa_userid = ${req.params.csiswaId}`
            db.query(sql, function (err, result) {
                if (err) {
                    res.status(err.code)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: err.code,
                        message: err.message
                    })
                } else {
                    res.status(200)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: 200,
                        message: 'Hapus data siswa berhasil'
                    })
                }
            })
        })
    })

pendaftaranRouter.route("/")
    .get(middleware.isAdmin, (req, res) => {
        db.connect(function (err) {
            let sql = `SELECT * FROM m_csiswa`
            db.query(sql, function (err, result) {
                if (err) {
                    res.status(err.code)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: err.code,
                        message: err.message
                    })
                } else {
                    res.status(200)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: 200,
                        message: "Berhasil ambil list siswa",
                        data: result
                    })
                }
            })
        })
    })
    .post((req, res) => {
        db.connect(function (err) {
            if (req.body.nama == null || req.body.nama == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'nama kosong!'
                })
            }
            if (req.body.foto == null || req.body.foto == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'foto kosong!'
                })
            }
            if (req.body.alamat == null || req.body.alamat == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'alamat kosong!'
                })
            }
            if (req.body.notelp == null || req.body.notelp == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'nomor telepon kosong!'
                })
            }
            if (req.body.sekolah_asal == null || req.body.sekolah_asal == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'sekolah asal kosong!'
                })
            }
            if (req.body.nama_orang_tua == null || req.body.nama_orang_tua == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'nama orang tua kosong!'
                })
            }
            if (req.body.userid == null || req.body.userid == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'id kosong!'
                })
            }
            let sql =
                `INSERT INTO m_csiswa (csiswa_nama, csiswa_foto, csiswa_alamat, csiswa_notelp, ciswa_sekolahasal, csiswa_namaorangtua, csiswa_userid) VALUES ('${req.body.nama}','${req.body.foto}','${req.body.alamat}','${req.body.notelp}','${req.body.sekolah_asal}','${req.body.nama_orang_tua}','${req.body.userid}')`
            db.query(sql, function (err, result) {
                if (err) {
                    res.status(err.code)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: err.code,
                        message: err.message
                    })
                } else {
                    res.status(200)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: 200,
                        message: "Berhasil tambah siswa"
                    })
                }
            })
        })
    })
    .put((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method POST'
        })
    })
    .delete((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method DELETE'
        })
    })

module.exports = pendaftaranRouter;