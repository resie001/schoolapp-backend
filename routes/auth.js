var db = require("../database/db_config");
const express = require("express");
const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");
const { json } = require("body-parser");
require('dotenv').config();
const saltRounds = 10;

const authRouter = express.Router()
authRouter.use(express.json())

authRouter.route("/login")
    .get((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method GET'
        })
    })
    .put((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method PUT'
        })
    })
    .delete((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method DELETE'
        })
    })
    .post((req, res) => {
        db.connect(function (err) {
            if (req.body.email == null || req.body.email == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'email kosong!'
                })
            }
            if (req.body.password == null || req.body.password == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'password kosong!'
                })
            }
            let sql = `SELECT * FROM m_user WHERE user_email='${req.body.email}'`
            db.query(sql, function (err, result) {
                if (result.length == 1) {
                    if (result[0].user_role == 1) {
                        if (result[0].user_password == req.body.password) {
                            var token = jwt.sign({ user: result[0] }, process.env.HASH)
                            if (token) {
                                res.status(200)
                                res.setHeader('Content-Type', 'application/json')
                                res.json({
                                    status: 200,
                                    message: 'Admin',
                                    data: {
                                        token: token,
                                        id: result[0].user_id,
                                        email: result[0].user_email
                                    }
                                })
                            } else {
                                res.status(400)
                                res.setHeader('Content-Type', 'application/json')
                                res.json({
                                    status: 400,
                                    message: 'token gagal dibuat'
                                })
                            }
                        } else {
                            res.status(400)
                            res.setHeader('Content-Type', 'application/json')
                            res.json({
                                status: 400,
                                message: 'password salah'
                            })
                        }
                    } else {
                        bcrypt.compare(req.body.password, result[0].user_password, function (err, result_hash) {
                            if (result_hash == true) {
                                var token = jwt.sign({ user: result[0] }, process.env.HASH)
                                if (token) {
                                    res.status(200)
                                    res.setHeader('Content-Type', 'application/json')
                                    res.json({
                                        status: 200,
                                        message: 'Siswa',
                                        data: {
                                            token: token,
                                            id: result[0].user_id,
                                            email: result[0].user_email
                                        }
                                    })
                                } else {
                                    res.status(400)
                                    res.setHeader('Content-Type', 'application/json')
                                    res.json({
                                        status: 400,
                                        message: 'token gagal dibuat'
                                    })
                                }
                            } else {
                                res.status(400)
                                res.setHeader('Content-Type', 'application/json')
                                res.json({
                                    status: 400,
                                    message: 'password salah'
                                })
                            }
                        });
                    }
                } else {
                    res.status(400)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: 400,
                        message: 'Email tidak ada'
                    })
                }
            })
        })
    })

authRouter.route("/register")
    .post((req, res) => {
        db.connect(function (err, result) {
            if (req.body.email == null || req.body.email == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'email kosong!'
                })
                return
            }
            if (req.body.password == null || req.body.password == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'password kosong!'
                })
                return
            }
            bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
                var sql = `INSERT INTO m_user (user_email, user_password) VALUES ('${req.body.email}','${hash}')`
                if (err) {
                    res.status(400)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: 400,
                        message: err
                    })
                } else {
                    db.query(sql, function (err, result) {
                        if (err) {
                            res.status(400)
                            res.setHeader('Content-Type', 'application/json')
                            res.json({
                                status: 400,
                                message: err
                            })
                        } else {
                            res.status(200)
                            res.setHeader('Content-Type', 'application/json')
                            res.json({
                                status: 200,
                                message: 'Berhasil registrasi!'
                            })
                        }
                    })
                }
            });
        })
    })
    .get((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method GET'
        })
    })
    .put((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method PUT'
        })
    })
    .delete((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method DELETE'
        })
    })

module.exports = authRouter;