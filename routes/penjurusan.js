var db = require("../database/db_config");
const express = require("express");
const middleware = require("../middleware/middleware")

const penjurusanRouter = express.Router()
penjurusanRouter.use(express.json())

penjurusanRouter.route("/")
    .get((req, res) => {
        db.connect(function (err) {
            let sql = "SELECT * FROM m_penjurusan"
            db.query(sql, function (err, result) {
                if (err) {
                    res.status(err.code)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: err.code,
                        message: err.message
                    })
                } else {
                    res.status(200)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: 200,
                        message: "Berhasil ambil list penjurusan",
                        data: result
                    })
                }
            })
        })
    })
    .post(middleware.isAdmin, (req, res) => {
        db.connect(function (err) {
            if (req.body.nama == null || req.body.nama == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'nama kosong!'
                })
            }
            if (req.body.kuota == null || req.body.kuota == "" || req.body.kuota == 1) {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'kuota kosong!'
                })
            }
            if (req.body.status == null || req.body.status == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'status kosong!'
                })
            }
            let sql = `INSERT INTO m_penjurusan (penjurusan_nama, penjurusan_kuota) VALUES ('${req.body.nama}','${req.body.kuota}')`
            db.query(sql, function (err, result) {
                if (err) {
                    res.status(err.code)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: err.code,
                        message: err.message
                    })
                } else {
                    res.status(200)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: 200,
                        message: "Berhasil tambah jurusan"
                    })
                }
            })
        })
    })
    .put(middleware.isAdmin, (req, res) => {
        db.connect(function (err) {
            if (req.body.id == null || req.body.id == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'id kosong!'
                })
            }
            if (req.body.nama == null || req.body.nama == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'nama kosong!'
                })
            }
            if (req.body.status == null || req.body.status == "") {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'status kosong!'
                })
            }
            if (req.body.kuota == null || req.body.kuota == "" || req.body.kuota == 1) {
                res.status(400)
                res.setHeader('Content-Type', 'application/json')
                res.json({
                    status: 400,
                    message: 'kuota kosong!'
                })
            }
            let sql = `UPDATE m_penjurusan SET penjurusan_nama = '${req.body.nama}' , penjurusan_kuota = '${req.body.kuota}' , penjurusan_status = '${req.body.status}' WHERE penjurusan_id = '${req.body.id}'`
            db.query(sql, function (err, result) {
                if (err) {
                    res.status(err.code)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: err.code,
                        message: err.message
                    })
                } else {
                    res.status(200)
                    res.setHeader('Content-Type', 'application/json')
                    res.json({
                        status: 200,
                        message: "Berhasil update jurusan"
                    })
                }
            })
        })
    })
    .delete((req, res) => {
        res.status(400)
        res.setHeader('Content-Type', 'application/json')
        res.json({
            status: 400,
            message: 'tidak support method DELETE'
        })
    })

module.exports = penjurusanRouter;